EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74hc4511:74HC4511 IC?
U 1 1 61B3F32D
P 4250 3050
AR Path="/61B3F123/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61B4A8D6/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61B4C5F4/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61B4CAA8/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61B4CAB1/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61BB0314/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61BB1F9F/61B3F32D" Ref="IC?"  Part="1" 
AR Path="/61BB3BD9/61B3F32D" Ref="IC?"  Part="1" 
F 0 "IC?" H 4250 3681 50  0000 C CNN
F 1 "74HC4511" H 4250 3590 50  0000 C CNN
F 2 "" H 3900 2650 50  0001 C CNN
F 3 "" H 3900 2650 50  0001 C CNN
	1    4250 3050
	1    0    0    -1  
$EndComp
Text HLabel 3650 3350 0    50   Input ~ 0
~LE
Text HLabel 3650 2750 0    50   Input ~ 0
A
Text HLabel 3650 2850 0    50   Input ~ 0
B
Text HLabel 3650 2950 0    50   Input ~ 0
C
Text HLabel 3650 3050 0    50   Input ~ 0
D
Wire Wire Line
	3750 3250 3700 3250
Wire Wire Line
	3700 3250 3700 3150
Wire Wire Line
	3700 3150 3750 3150
Wire Wire Line
	3650 3050 3750 3050
Wire Wire Line
	3750 2950 3650 2950
Wire Wire Line
	3650 2850 3750 2850
Wire Wire Line
	3750 2750 3650 2750
Wire Wire Line
	3650 3350 3750 3350
Text HLabel 4250 3600 3    50   Input ~ 0
GND
$Comp
L Display_Character:HDSP-7501 U?
U 1 1 61B44243
P 6450 3050
AR Path="/61B3F123/61B44243" Ref="U?"  Part="1" 
AR Path="/61B4A8D6/61B44243" Ref="U?"  Part="1" 
AR Path="/61B4C5F4/61B44243" Ref="U?"  Part="1" 
AR Path="/61B4CAA8/61B44243" Ref="U?"  Part="1" 
AR Path="/61B4CAB1/61B44243" Ref="U?"  Part="1" 
AR Path="/61BB0314/61B44243" Ref="U?"  Part="1" 
AR Path="/61BB1F9F/61B44243" Ref="U?"  Part="1" 
AR Path="/61BB3BD9/61B44243" Ref="U?"  Part="1" 
F 0 "U?" H 6450 3717 50  0000 C CNN
F 1 "HDSP-7501" H 6450 3626 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A151" H 6450 2500 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 5950 3600 50  0001 C CNN
	1    6450 3050
	1    0    0    -1  
$EndComp
Text HLabel 4250 2350 1    50   Input ~ 0
5V
Wire Wire Line
	4850 2750 4750 2750
Wire Wire Line
	4850 2850 4750 2850
Wire Wire Line
	4850 2950 4750 2950
Wire Wire Line
	4850 3050 4750 3050
Wire Wire Line
	4850 3150 4750 3150
Wire Wire Line
	4850 3250 4750 3250
Wire Wire Line
	4850 3350 4750 3350
$Comp
L Transistor_Array:ULN2003A U?
U 1 1 61B893A3
P 5250 2950
AR Path="/61B4A8D6/61B893A3" Ref="U?"  Part="1" 
AR Path="/61BB0314/61B893A3" Ref="U?"  Part="1" 
AR Path="/61BB1F9F/61B893A3" Ref="U?"  Part="1" 
AR Path="/61BB3BD9/61B893A3" Ref="U?"  Part="1" 
F 0 "U?" H 5250 3617 50  0000 C CNN
F 1 "ULN2003A" H 5250 3526 50  0000 C CNN
F 2 "" H 5300 2400 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 5350 2750 50  0001 C CNN
	1    5250 2950
	1    0    0    -1  
$EndComp
Text HLabel 3650 3150 0    50   Input ~ 0
5V
Wire Wire Line
	4250 2350 4250 2600
Wire Wire Line
	3650 3150 3700 3150
Connection ~ 3700 3150
Wire Wire Line
	4250 3600 4250 3500
Text HLabel 5250 3650 3    50   Input ~ 0
GND
Wire Wire Line
	5250 3650 5250 3550
$Comp
L Device:R R?
U 1 1 61B909D1
P 5900 2750
AR Path="/61B4A8D6/61B909D1" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B909D1" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B909D1" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B909D1" Ref="R?"  Part="1" 
F 0 "R?" V 5693 2750 50  0000 C CNN
F 1 "250R" V 5784 2750 50  0000 C CNN
F 2 "" V 5830 2750 50  0001 C CNN
F 3 "~" H 5900 2750 50  0001 C CNN
	1    5900 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 2750 5750 2750
Wire Wire Line
	6050 2750 6150 2750
$Comp
L Device:R R?
U 1 1 61B93A23
P 5900 2850
AR Path="/61B4A8D6/61B93A23" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B93A23" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B93A23" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B93A23" Ref="R?"  Part="1" 
F 0 "R?" V 5693 2850 50  0000 C CNN
F 1 "250R" V 5784 2850 50  0000 C CNN
F 2 "" V 5830 2850 50  0001 C CNN
F 3 "~" H 5900 2850 50  0001 C CNN
	1    5900 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 2850 5750 2850
Wire Wire Line
	6050 2850 6150 2850
$Comp
L Device:R R?
U 1 1 61B94A92
P 5900 2950
AR Path="/61B4A8D6/61B94A92" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B94A92" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B94A92" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B94A92" Ref="R?"  Part="1" 
F 0 "R?" V 5693 2950 50  0000 C CNN
F 1 "250R" V 5784 2950 50  0000 C CNN
F 2 "" V 5830 2950 50  0001 C CNN
F 3 "~" H 5900 2950 50  0001 C CNN
	1    5900 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 2950 5750 2950
Wire Wire Line
	6050 2950 6150 2950
$Comp
L Device:R R?
U 1 1 61B94A9A
P 5900 3050
AR Path="/61B4A8D6/61B94A9A" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B94A9A" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B94A9A" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B94A9A" Ref="R?"  Part="1" 
F 0 "R?" V 5693 3050 50  0000 C CNN
F 1 "250R" V 5784 3050 50  0000 C CNN
F 2 "" V 5830 3050 50  0001 C CNN
F 3 "~" H 5900 3050 50  0001 C CNN
	1    5900 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3050 5750 3050
Wire Wire Line
	6050 3050 6150 3050
$Comp
L Device:R R?
U 1 1 61B96A42
P 5900 3150
AR Path="/61B4A8D6/61B96A42" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B96A42" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B96A42" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B96A42" Ref="R?"  Part="1" 
F 0 "R?" V 5693 3150 50  0000 C CNN
F 1 "250R" V 5784 3150 50  0000 C CNN
F 2 "" V 5830 3150 50  0001 C CNN
F 3 "~" H 5900 3150 50  0001 C CNN
	1    5900 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3150 5750 3150
Wire Wire Line
	6050 3150 6150 3150
$Comp
L Device:R R?
U 1 1 61B96A4A
P 5900 3250
AR Path="/61B4A8D6/61B96A4A" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B96A4A" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B96A4A" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B96A4A" Ref="R?"  Part="1" 
F 0 "R?" V 5693 3250 50  0000 C CNN
F 1 "250R" V 5784 3250 50  0000 C CNN
F 2 "" V 5830 3250 50  0001 C CNN
F 3 "~" H 5900 3250 50  0001 C CNN
	1    5900 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3250 5750 3250
Wire Wire Line
	6050 3250 6150 3250
$Comp
L Device:R R?
U 1 1 61B96A52
P 5900 3350
AR Path="/61B4A8D6/61B96A52" Ref="R?"  Part="1" 
AR Path="/61BB0314/61B96A52" Ref="R?"  Part="1" 
AR Path="/61BB1F9F/61B96A52" Ref="R?"  Part="1" 
AR Path="/61BB3BD9/61B96A52" Ref="R?"  Part="1" 
F 0 "R?" V 5693 3350 50  0000 C CNN
F 1 "250R" V 5784 3350 50  0000 C CNN
F 2 "" V 5830 3350 50  0001 C CNN
F 3 "~" H 5900 3350 50  0001 C CNN
	1    5900 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3350 5750 3350
Wire Wire Line
	6050 3350 6150 3350
Text HLabel 6850 3250 1    50   Input ~ 0
12V
Wire Wire Line
	6750 3350 6850 3350
Wire Wire Line
	6850 3350 6850 3250
Wire Wire Line
	6750 3450 6850 3450
Wire Wire Line
	6850 3450 6850 3350
Connection ~ 6850 3350
$EndSCHEMATC
