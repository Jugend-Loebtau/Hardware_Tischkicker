# Nützlich für Git

- [Markdown (.md) Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "Markdown-Cheatsheet")

# Elektronik

## LED Streifen

- [Datenblatt WS2812B](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf "Datenblatt WS2812B")
- [Anleitung Ansteuerung mit Rapsi](https://core-electronics.com.au/tutorials/ws2812-addressable-leds-raspberry-pi-quickstart-guide.html "Anleitung")
