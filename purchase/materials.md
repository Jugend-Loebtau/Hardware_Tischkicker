# Baumaterial:
* Holzi

# Elektronik:
* 2x [Lichtsensor](https://www.reichelt.de/lichtsensor-2-2-7-v-hmm-s4810-p247685.html?&trstct=pos_3)
* 2x [Lichtsensor-pullups](https://www.reichelt.de/widerstand-kohleschicht-10-kohm-0411-500-mw-5-k-o-rd12jn103t52-p237431.html?&trstct=pol_0)
* 2x [Lichtschranken LED's](https://www.reichelt.de/ir-led-850-nm-5-mm-t1-3-4-hir-333-a-p216807.html?&trstct=pos_4)
* 2x [Doppelpunkt LED's](https://www.reichelt.de/led-3-mm-bedrahtet-rot-250-mcd-40-rnd-135-00008-p224083.html?&trstct=pol_0)
* 4x [Lichtschranken u. Doppelpunkt vorwiderstände](https://www.reichelt.de/widerstand-kohleschicht-100-ohm-0411-500-mw-5-k-o-rd12jn101t52-p237433.html?&trstct=pol_0)

* 4x [BCD-Decoder](https://www.reichelt.de/latch-7-segment-2-6-v-dil-16-cd-74hc4511e-tex-p216880.html?&trstct=pol_4)
* 4x [7-Segment Dsp.](https://www.reichelt.de/7-segment-anzeige-rot-56-9mm-gem-anode-sa-23-12-rt-p31577.html?&trstct=lsbght_sldr::31578)
* 4x [Verstärker](https://www.reichelt.de/darlington-arrays-konstantstromregler-dip-16-uln-2003-an-p189138.html?&trstct=pos_4)
* 7x [Widerstandsnetzwerke](https://www.reichelt.de/widerstandsnetzwerk-einzelwiderstaende-270-ohm-4wid-8pins-sil-8-4-270-p17922.html?&trstct=pol_0)

* 10x [Entstörkondensatoren](https://www.reichelt.de/vielschicht-keramikkondensator-100n-20-z5u-2-5-100n-p22977.html?&trstct=pol_1)
* 1x [Pegelwandler](https://www.reichelt.de/transceiver-octal-4-75-5-25-v-dip-20-ls-245-p10667.html?&trstct=pos_0)

* 1x [rpi-anschluss:](https://www.reichelt.de/buchsenleisten-2-54-mm-2x20-gerade-mpe-094-2-040-p199544.html?&trstct=pol_0)

-> Links an einigen Stellen nicht mehr aktuell